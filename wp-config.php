<?php

// For  Local Development use a wp-config-local.php file
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
    require_once( dirname( __FILE__ ) . '/wp-config-local.php');
} else {
    // If no wp-config-local.php use these credentials
    define('DB_NAME',          'meetup_wp');
    define('DB_USER',          'meetup_sa');
    define('DB_PASSWORD',      'CJ^ggqB+kTNBvg5Q5{+5*=5-Vzfv=}3@');
    define('DB_HOST',          'localhost');
    define('DB_CHARSET',       'utf8');
    define('DB_COLLATE',       '');

    // Disable Wordpress/Plugin and Theme Updates
    define( 'AUTOMATIC_UPDATER_DISABLED', true );
    define( 'WP_AUTO_UPDATE_CORE', false );
    define( 'DISALLOW_FILE_MODS', true );

    // Turn off debug on live environment
    define('WP_DEBUG', false);
    define('WP_DEBUG_DISPLAY', false);
    define('WP_DEBUG_LOG', false);
}

/* Global wp-config stuff here */

// Generate new salt here https://api.wordpress.org/secret-key/1.1/salt/
define('AUTH_KEY',         'pC6]5A$%fjAu1HQlw;l$ gkEZ6 tG+6>)xR;OcZGs$:1!sL8CNOl[H)[U6U jp(F');
define('SECURE_AUTH_KEY',  'S[gQH-!XLgytpnLOgrT^5em.F`YyHZ)%!]xA?MW=|+0mG)p1fvpMRI_DU,3^T29u');
define('LOGGED_IN_KEY',    'Z72LCDtPE<wmml<n#07aYr0N|,q5`Sf.L>{SB`ThNDK-BP%|Zfb*FA^;#l4N9,D#');
define('NONCE_KEY',        '9X>A,P]sghQeK3*/ o>d/MSoB+lE)6v`ibvX<VCxeOw?;sZzxK`GDU6tz*.65Gnz');
define('AUTH_SALT',        'N~dZ8wMg4mox~&j2)|]Tb0T9::aJZUf6Bry@8{[fI6C ]QxF5mOtKWG:.O7gBwlu');
define('SECURE_AUTH_SALT', '#.V~R:[SH<NQD2w/ca1C37Si4-#{Zl`]:m|zBDwT=qD@(u(*?Q8iPZ:VVLf/=@@G');
define('LOGGED_IN_SALT',   ';F|%6P16Y^A2xkYKw+Pc%CpW_61[}V|ce|dA`V#Vtr/n8Ma&_Pvuvwu{;$hxE-#`');
define('NONCE_SALT',       '2S&W]kLO?,T!0-u&$f5(#NL(+tBu(*Fd+.02&3|N3P2lBYz2.$ESt3#+J?prRh(#');

$table_prefix = 'wp_';

define('WPLANG', '');

if ( ! defined( 'WP_DEBUG' ) ) {
    define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy Pressing. */

if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');